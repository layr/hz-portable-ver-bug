package hz.bug.app.repo;

import java.util.function.BiFunction;

import org.springframework.stereotype.Component;

import static com.google.common.collect.Sets.union;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

import hz.bug.app.model.ModelA;
import hz.bug.app.config.HzConf.Maps;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@RequiredArgsConstructor
public class Repo {

    private static final Combiner COMBINER = new Combiner();

    private final HazelcastInstance hz;

    public void add( final ModelA model ) {
        log.info( "adding {}...", model );

        // TODO: note our own merge() is effectively same as calling this:
//        getModelAMap().merge(
//                model.getId(),
//                model,
//                COMBINER );

        while ( !merge( model.getId(), model, COMBINER ) ) {
            log.error( "ModelA Merge failed - will sleep and try again... " );
            sleep( 100L );
        }

        log.info( "addition done" );
    }

    public ModelA get( final String eventId ) {
        return getModelAMap().get( eventId );
    }

    private boolean merge( final String key, final ModelA value, final Combiner c ) {
        IMap<String, ModelA> map = getModelAMap();

        ModelA oldValue = map.get( key );
        log.info( "merge : old value - {}", oldValue );
        if ( oldValue != null ) {
            ModelA newValue = c.apply( oldValue, value );
            log.info( "merge : new value - {}", newValue );
            if ( newValue != null ) {
                return map.replace( key, oldValue, newValue );
            } else {
                return map.remove( key, oldValue );
            }
        } else {
            if ( map.putIfAbsent( key, value ) == null ) {
                return true;
            }
        }

        return false;
    }

    private void sleep( long ms ) {
        try {
            Thread.sleep( ms );
        } catch ( InterruptedException e ) {
            log.warn( "Sleep interrupted : {}", e.getMessage() );
        }
    }

    private IMap<String, ModelA> getModelAMap() {
        return hz.getMap( Maps.HC_MODEL_A_MAP );
    }

    private static class Combiner implements BiFunction<ModelA, ModelA, ModelA> {

        @Override
        public ModelA apply( final ModelA left, final ModelA right ) {
            return ModelA.builder()
                    .id( left.getId() )
                    .strCollection( union( left.getStrCollection(), right.getStrCollection() ) )
                    .build();
        }
    }

}

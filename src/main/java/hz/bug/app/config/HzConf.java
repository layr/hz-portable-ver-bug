package hz.bug.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import hz.bug.app.model.portable.PortableDataInit;
import lombok.experimental.UtilityClass;

import static java.util.Collections.singletonList;

@Configuration
public class HzConf {

    @Bean
    @Profile( "server" )
    public Config config() {
        Config c = new Config();

        JoinConfig joinConfig = c.getNetworkConfig().getJoin();
        joinConfig.getMulticastConfig().setEnabled( false );
        joinConfig.getTcpIpConfig().setEnabled( true ).setMembers( singletonList( "127.0.0.1" ) );

        c.setInstanceName( "hz-client-server-merge-bug" );
        PortableDataInit.configure( c );
        return c;
    }

    @Bean
    @Profile( "server" )
    public HazelcastInstance hz( final Config conf ) {
        return Hazelcast.newHazelcastInstance( conf );
    }

    @Bean
    @Profile( "client" )
    public ClientConfig clientConf() {
        ClientConfig c = new ClientConfig();
        c.addAddress( "127.0.0.1:5701" );
        PortableDataInit.configure( c );
        return c;
    }

    @Bean
    @Profile( "client" )
    public HazelcastInstance hz( final ClientConfig conf ) {
        return HazelcastClient.newHazelcastClient( conf );
    }

    @UtilityClass
    public static class Maps {
        public String HC_MODEL_A_MAP = "/repo/modelA";
    }

}

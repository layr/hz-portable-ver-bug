package hz.bug.app.api;

import java.util.Set;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import hz.bug.app.repo.Repo;
import hz.bug.app.model.ModelA;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static hz.bug.app.api.ApiController.ROOT;

@RestController
@RequestMapping( path = ROOT, produces = MediaType.APPLICATION_JSON_VALUE )
@Slf4j
@RequiredArgsConstructor
public class ApiController {

    public static final String ROOT = "/api";

    private final Repo repo;

    @GetMapping( path = "/modela/{id}" )
    @ResponseBody
    public ResponseEntity<ModelA> getMapping(
            @PathVariable final String id ) {
        log.info( "GET ModelA for [{}]", id );
        ModelA m = repo.get( id );
        if ( m == null ) {
            return new ResponseEntity<>( HttpStatus.NOT_FOUND );
        }

        return new ResponseEntity<>( repo.get( id ), HttpStatus.OK );
    }

    @PostMapping( path = "/modela/{id}/{collection}" )
    @ResponseBody
    @ResponseStatus( code = HttpStatus.OK )
    public void getStreamProviders(
            @PathVariable final String id,
            @PathVariable final Set<String> collection
    ) {
        ModelA m = ModelA.builder()
                .id( id )
                .strCollection( collection )
                .build();
        log.info( "PUT ModelA: [{}]", m );

        repo.add( m );
    }
}

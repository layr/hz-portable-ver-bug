package hz.bug.app.model.portable;

import java.util.Map;

import com.google.common.collect.Maps;
import com.hazelcast.nio.serialization.Portable;

import hz.bug.app.model.ModelA;
import hz.bug.app.model.ModelB;
import lombok.Getter;

import static java.util.Arrays.asList;

@Getter
public enum PortableDataClassId {

    MODEL_A( 1, ModelA.class ),
    MODEL_B( 2, ModelB.class );

    private final int id;
    private final Class<?> clazz;

    private static final Map<Integer, PortableDataClassId> LOOKUP =
            Maps.uniqueIndex( asList( values() ), PortableDataClassId::getId );

    <K extends Portable> PortableDataClassId( final int id, final Class<K> clazz ) {
        this.id = id;
        this.clazz = clazz;
    }

    public static PortableDataClassId valueOfId( final int id ) {
        return LOOKUP.get( id );
    }
}

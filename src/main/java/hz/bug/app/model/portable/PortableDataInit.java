package hz.bug.app.model.portable;

import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.config.Config;
import com.hazelcast.config.SerializationConfig;

import hz.bug.app.model.ModelA;
import hz.bug.app.model.ModelB;
import lombok.experimental.UtilityClass;

@UtilityClass
public class PortableDataInit {

    public static void configure( final ClientConfig config ) {
        configure( config.getSerializationConfig() );
    }

    public static void configure( final Config config ) {
        configure( config.getSerializationConfig() );
    }

    public static void configure( final SerializationConfig config ) {
        config.addPortableFactory( PortableDataFactory.ID, new PortableDataFactory() );
        config.setPortableVersion( PortableDataFactory.PORTABLE_VER );
        registerClassDefs( config );
    }

    public static void registerClassDefs( final SerializationConfig config ) {
        config.addClassDefinition( ModelA.CLASS_DEFINITION );
        config.addClassDefinition( ModelB.CLASS_DEFINITION );
    }
}

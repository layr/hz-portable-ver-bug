package hz.bug.app.model.portable;

import com.hazelcast.nio.serialization.Portable;
import com.hazelcast.nio.serialization.PortableFactory;

public class PortableDataFactory implements PortableFactory {

    public static final int ID = 1;  // factory ID

    /**
     * This is the version of our {@link Portable} classes.
     */
    public static final int PORTABLE_VER = 1;

    @Override
    public Portable create( final int classId ) {
        final PortableDataClassId value = PortableDataClassId.valueOfId( classId );
        if ( value != null ) {
            try {
                return (Portable) value.getClazz().newInstance();
            } catch ( final InstantiationException | IllegalAccessException e ) {
                throw new RuntimeException(
                        String.format( "Error instantiating portable with class [%d]:[%s]", classId, value.toString() ), e
                );
            }
        } else {
            throw new RuntimeException( "Portable with class id [" + classId + "] not supported by this factory" );
        }
    }
}

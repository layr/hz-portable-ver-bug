package hz.bug.app.model.portable;

import java.io.IOException;
import java.util.Collection;

import static com.google.common.collect.ImmutableList.toImmutableList;
import static com.google.common.collect.ImmutableSet.toImmutableSet;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.hazelcast.nio.serialization.PortableReader;
import com.hazelcast.nio.serialization.PortableWriter;

import static java.util.Arrays.stream;

public class PortableUtils {

    private PortableUtils() {
    }

    public static <K extends Enum<K>> void writeEnum( final PortableWriter writer, final String name, final K value ) throws IOException {
        writer.writeUTF( name, value != null ? value.name() : null );
    }

    public static <K extends Enum<K>> K readEnum( final PortableReader reader, final String name, final Class<K> clazz ) throws IOException {
        final String value = reader.readUTF( name );
        if ( value != null ) {
            return Enum.valueOf( clazz, value );
        } else {
            return null;
        }
    }

    public static <K extends Enum<K>> void writeEnumCollection( final PortableWriter writer, final String name, final Collection<K> values )
            throws IOException {
        if ( values != null ) {
            String[] valueNames = values.stream().map( Enum::name ).toArray( String[]::new );
            writer.writeUTFArray( name, valueNames );
        } else {
            writer.writeUTFArray( name, null );
        }
    }

    public static <K extends Enum<K>> ImmutableList<K> readEnumList( final PortableReader reader, final String name, final Class<K> clazz ) throws IOException {
        final String[] values = reader.readUTFArray( name );
        if ( values != null ) {
            return stream( values )
                    .map( s -> Enum.valueOf( clazz, s ) )
                    .collect( toImmutableList() );
        } else {
            return null;
        }
    }

    public static <K extends Enum<K>> ImmutableSet<K> readEnumSet( final PortableReader reader, final String name, final Class<K> clazz ) throws IOException {
        final String[] values = reader.readUTFArray( name );
        if ( values != null ) {
            return stream( values )
                    .map( s -> Enum.valueOf( clazz, s ) )
                    .collect( toImmutableSet() );
        } else {
            return null;
        }
    }

    public static void writeStringCollection( final PortableWriter writer, final String name, final Collection<String> values )
            throws IOException {
        if ( values != null ) {
            writer.writeUTFArray( name, values.toArray( new String[values.size()] ) );
        } else {
            writer.writeUTFArray( name, null );
        }
    }

    public static ImmutableList<String> readStringList( final PortableReader reader, final String name ) throws IOException {
        final String[] values = reader.readUTFArray( name );
        return ( values == null ) ? null : ImmutableList.copyOf( values );
    }

    public static ImmutableSet<String> readStringSet( final PortableReader reader, final String name ) throws IOException {
        final String[] values = reader.readUTFArray( name );
        return ( values == null ) ? null : ImmutableSet.copyOf( values );
    }
}

package hz.bug.app.model;

import java.io.IOException;
import java.io.Serializable;

import com.hazelcast.nio.serialization.*;

import hz.bug.app.model.portable.PortableDataClassId;
import hz.bug.app.model.portable.PortableDataFactory;
import lombok.*;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

import static hz.bug.app.model.portable.PortableDataFactory.PORTABLE_VER;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@FieldNameConstants
public class ModelB implements Serializable, Portable {

    private static final long serialVersionUID = 1L;
    private static final int PORTABLE_ID = PortableDataClassId.MODEL_B.getId();

    public static final ClassDefinition CLASS_DEFINITION = new ClassDefinitionBuilder( PortableDataFactory.ID, PORTABLE_ID, PORTABLE_VER )
            .addUTFArrayField( FIELD_STR_ARR )
            .build();

    private String[] strArr;

    @Override
    public int getFactoryId() {
        return PortableDataFactory.ID;
    }

    @Override
    public int getClassId() {
        return PORTABLE_ID;
    }

    @Override
    public void writePortable( final PortableWriter writer ) throws IOException {
        writer.writeUTFArray( FIELD_STR_ARR, strArr );
    }

    @Override
    public void readPortable( final PortableReader reader ) throws IOException {
        strArr = reader.readUTFArray( FIELD_STR_ARR );
    }
}

package hz.bug.app.model;

import java.io.IOException;
import java.io.Serializable;
import java.util.Set;

import com.hazelcast.nio.serialization.*;

import hz.bug.app.model.portable.PortableDataClassId;
import hz.bug.app.model.portable.PortableDataFactory;
import lombok.*;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

import static hz.bug.app.model.portable.PortableDataFactory.PORTABLE_VER;
import static hz.bug.app.model.portable.PortableUtils.readStringSet;
import static hz.bug.app.model.portable.PortableUtils.writeStringCollection;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
@FieldNameConstants
public class ModelA implements Serializable, Portable {

    private static final long serialVersionUID = 1L;
    private static final int PORTABLE_ID = PortableDataClassId.MODEL_A.getId();

    public static final ClassDefinition CLASS_DEFINITION = new ClassDefinitionBuilder( PortableDataFactory.ID, PORTABLE_ID, PORTABLE_VER )
            .addUTFField( FIELD_ID )
            .addUTFArrayField( FIELD_STR_COLLECTION )
            .build();

    private String id;
    private Set<String> strCollection;

    @Override
    public int getFactoryId() {
        return PortableDataFactory.ID;
    }

    @Override
    public int getClassId() {
        return PORTABLE_ID;
    }

    @Override
    public void writePortable( final PortableWriter writer ) throws IOException {
        writer.writeUTF( FIELD_ID, id );
        writeStringCollection( writer, FIELD_STR_COLLECTION, strCollection );
    }

    @Override
    public void readPortable( final PortableReader reader ) throws IOException {
        id = reader.readUTF( FIELD_ID );
        strCollection = readStringSet( reader, FIELD_STR_COLLECTION );
    }
}

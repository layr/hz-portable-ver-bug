# hz-portable-ver-bug

1. run server:
`./mvnw spring-boot:run -Dspring.profiles.active=server -Dserver.port=8082`
2. add test model from terminal:
`curl -X POST 'localhost:8082/api/modela/id1/s1,s2,s3'`
3. verify data is stored:
`curl 'localhost:8082/api/modela/id1/'`
4. from another terminal, launch client:
`./mvnw spring-boot:run -Dspring.profiles.active=client -Dserver.port=8083`
5. verify data can be accessed:
`curl 'localhost:8083/api/modela/id1/'`
6. merge data from client:
`curl -X POST 'localhost:8083/api/modela/id1/s1,s2,s5'`
7. repeat step 5) to verify data was merged and stored
8. stop client
9. bump portable ver to 2 in `PortableDataFactory`
10. repeat step 4)
11. repeat step 6)
